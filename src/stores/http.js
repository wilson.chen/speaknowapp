class http {
  constructor(evn) {
    this.evn = evn;
  }
  setToken(token) {
    this.token = token;
  }
  get headers() {
    const _headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };
    if (this.token) {
      return {
        ..._headers,
        token: this.token
      };
    }

    return _headers;
  }
  async post(url, params) {
    const location = `${this.evn}${url}`;
    const res = await fetch(location, {
      method: "POST",
      headers: this.headers,
      body: JSON.stringify(params)
    });
    return res.json();
  }
  async get(url, params) {
    const location = `${this.evn}${url}`;
    const res = await fetch(location, {
      method: "GET",
      headers: this.headers,
      body: JSON.stringify(params)
    });
    return res.json();
  }
}
export default http;
