import Message from "./Message";

import { AsyncStorage } from "react-native";
class Auth {
  constructor(http) {
    this.http = http;
  }
  setUser(user) {
    this.user = user;
  }
  async reloadUser() {
    const res = await this.http.get("/getUserInfo");
    if (res.result === "SUCCESS") {
      this.setUser(res.user);
    }
  }
  setToken(token) {
    AsyncStorage.setItem("token", token);
    this.http.setToken(token);
  }
  async getToken() {
    const res = await AsyncStorage.getItem("token");
    return res;
  }
  async updateProfile(signature) {
    const res = await this.http.post("/updateProfile", {
      signature
    });
    if (res.result === "SUCCESS") {
      this.user = {
        ...this.user,
        ...res.profile
      };
    }
    return res;
  }

  logout() {
    AsyncStorage.removeItem("token");
    this.http.setToken(null);
  }
  makeMessage(sender, receiver, data, type, status, timeStamp) {
    const mss = new Message({
      sender,
      receiver,
      data,
      type,
      status,
      host: this.user,
      timeStamp
    });
    return mss;
  }
}

export default Auth;
