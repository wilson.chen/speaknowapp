// @flow
import { observable, computed } from "mobx";
import _ from "lodash";
import moment from "moment";
import config from "../../Config";
import { getMessageTime } from "../utils/time";

const appConfig = config.appConfig;

class Room {
  @observable history = [];
  unReadMessages = observable.array([]);
  @observable tip;
  @computed
  get hasUnReadMessages() {
    return !_.isEmpty(this.unReadMessages);
  }
  get numberOfUnReadMessages() {
    if (this.hasUnReadMessages) {
      return this.unReadMessages.length;
    }
    return 0;
  }
  get lastMessage() {
    const { history, unReadMessages } = this;
    let lastMessage = null;
    if (!_.isEmpty(unReadMessages)) {
      lastMessage = unReadMessages[unReadMessages.length - 1];
    } else if (!_.isEmpty(history)) {
      lastMessage = history[history.length - 1];
    }
    return lastMessage;
  }
  @computed
  get historyWithTimeStamp() {
    const res = [];
    const pushTimeStampToRes = i => {
      const date = getMessageTime(this.history[i]);
      if (date) {
        res.push({
          type: "TIMESTAMP",
          value: date
        });
      }
    };
    _.forEach(this.history, (m, i) => {
      if (i === 0) {
        pushTimeStampToRes(0);
      } else if (m.timeStamp && this.history[i - 1].timeStamp) {
        if (
          m.timeStamp - this.history[i - 1].timeStamp >
          appConfig.MESSAGE_SHOW_TIMESTAMO_INTERVAL
        ) {
          pushTimeStampToRes(i);
        }
      }

      res.push(m);
    });
    return res;
  }
  constructor(target) {
    this.id = target._id;
    this.target = target;
  }
  addMessage(message) {
    if (message.read) {
      this.history.push(message);
      if (this.history.length > appConfig.MESSAGE_STORE_NUMBER) {
        this.history.replace(
          this.history.slice(-appConfig.MESSAGE_STORE_NUMBER)
        );
      }
    } else {
      this.unReadMessages.push(message);
    }
  }
  readAll() {
    this.history.replace(this.history.concat(this.unReadMessages));
    this.unReadMessages.replace([]);
  }
  clearHistory() {
    this.history.replace([]);
  }
}

export default Room;
