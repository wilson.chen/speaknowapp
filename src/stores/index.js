// @flow
import Auth from "./auth";
import PushInfo from "./push";
import House from "./house";
import Socket from "./socket";

import Http from "./http";
import config from "../../Config";

import React from "react";

export type Stores = {
  auth: Auth,
  http: Http,
  pushInfo: PushInfo,
  socket: Socket,
  house: House
};

export const configureStore = function() {
  const http = new Http(config.evn);
  const store = {
    auth: new Auth(http),
    http,
    pushInfo: new PushInfo(http),
    house: new House(http),
    socket: new Socket()
  };
  window.store = store;

  return store;
};

export const StoresContext = React.createContext<Stores>({});
