// @flow
import { observable, computed } from "mobx";
import Room from "./room";
import Message from "./Message";
import config from "../../Config";
import { map, filter, includes, isEmpty, find, findIndex } from "lodash";
import { AsyncStorage } from "react-native";

const appConfig = config.appConfig;

class house {
  rooms = observable.array([]);
  @observable friends = [];
  @observable currentNav = "TALK";
  @observable currentRoomId = null;
  host = null;
  constructor(http) {
    this.http = http;
  }
  @computed
  get currentRoom() {
    if (this.currentRoomId) {
      const res = find(this.rooms, m => m.id === this.currentRoomId);
      if (!res) {
        console.log(this.currentRoomId, this.rooms);
      }
      return res;
    }
    return null;
  }
  @computed
  get hasUnReadRoom() {
    return Boolean(find(this.rooms, r => r.hasUnReadMessages));
  }
  get numberOfAllUnReadMessages() {
    return reduce(this.rooms, (res, r) => r.numberOfUnReadMessages + res, 0);
  }

  directToRoom(userId: string) {
    const _roomIndex = findIndex(this.rooms, m => m.id === userId);
    const user = find(this.friends, f => f._id === userId);
    if (user) {
      if (_roomIndex === -1) {
        this.addRoom(user);
      } else {
        this.topRoom(userId);
      }
    }

    this.currentRoomId = userId;
    this.currentNav = "TALK";
  }
  topRoom(roomId: string) {
    const _roomIndex = findIndex(this.rooms, m => m.id === roomId);
    if (_roomIndex !== -1) {
      const copyRooms = this.rooms.slice();
      const _room = copyRooms.splice(_roomIndex, 1);
      copyRooms.push(_room[0]);
      this.rooms.replace(copyRooms);
    }
  }

  addRoom(user) {
    const room = new Room(user);
    this.rooms.push(room);
  }

  deleteRoom(user) {
    const index = findIndex(this.rooms, r => r.id === user._id);
    const res = this.rooms.splice(index, 1);
    if (this.currentRoomId === user.id) {
      this.currentRoomId = null;
    }
  }
  getUserFriends = async () => {
    const res = await this.http.get("/getUserFriends");
    if (res.result === "SUCCESS") {
      const friends = res.friends;
      this.setFriends(friends);
    }
  };
  setFriends(f) {
    this.friends = f;
  }
  addFriend(user) {
    this.friends.push(user);
  }
  deleteFriend(user) {
    const fs = this.friends.toJS();
    const index = findIndex(fs, f => f._id === user._id);
    const res = fs.splice(index, 1);
    this.friends.replace(res);
  }
  addMessage(mss) {
    if (this.currentRoomId === mss.target._id) {
      mss.read = true;
    }
    let room = find(this.rooms, m => m.id === mss.target._id);
    if (room) {
      room.addMessage(mss);
      this.topRoom(room.id);
    } else {
      const _room = new Room(mss.target);
      _room.addMessage(mss);
      this.rooms.push(_room);
    }

    this.cacheRooms();
  }

  clearRooms() {
    this.rooms.replace([]);
  }
  setHost(host) {
    this.host = host;
  }
  resetHouse() {
    this.rooms.replace([]);
    this.host = null;
  }
  async cacheRooms() {
    const hostId = this.host._id;
    const data = this.rooms.slice(0, appConfig.LOCALSTORAGE_ROOM_NUMBER);
    await AsyncStorage.setItem(hostId, JSON.stringify(data));
  }
  async initRooms() {
    const hostId = this.host._id;
    console.log("hostId:", this.host._id, this.host.accountName);
    const cachedRooms = await AsyncStorage.getItem(hostId);
    if (cachedRooms) {
      const validRooms = filter(JSON.parse(cachedRooms), cr => {
        return includes(this.host.friends, cr.id);
      });

      if (!isEmpty(validRooms)) {
        const cachedResult = map(validRooms, vr => {
          const newerTarget = find(this.friends, f => f._id === vr.target._id);
          const _room = new Room(newerTarget);
          _room.history.replace(
            map(vr.history, m => {
              const _mss = new Message(m);
              return _mss;
            })
          );
          return _room;
        });
        this.rooms.replace(cachedResult);
      }
    }
  }
}

export default house;
