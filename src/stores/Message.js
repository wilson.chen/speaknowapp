import { observable } from "mobx";
class Message {
  @observable history = [];
  @observable tip;
  constructor(options) {
    const { sender, receiver, data, type, status, host, timeStamp } = options;
    this.sender = sender;
    this.receiver = receiver;
    this.data = data;
    this.type = type;
    this.status = status;
    this.host = host;
    this.timeStamp = timeStamp;

    this.read = this.host._id === this.sender._id;
  }

  get isSentByHost() {
    return this.host._id === this.sender._id;
  }
  get target() {
    return this.isSentByHost ? this.receiver : this.sender;
  }
}

export default Message;
