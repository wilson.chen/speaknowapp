// @flow
import { observable, computed } from "mobx";
import { find, filter, map, forEach } from "lodash";
class PushInfo {
  @observable list = [];
  constructor(http) {
    this.http = http;
  }
  @computed
  get hasUnreadPush() {
    return Boolean(find(this.list, l => !l.read));
  }
  get unreadPushIds() {
    const unReadList = filter(this.list, l => !l.read);
    return map(unReadList, l => l.id);
  }
  async readAllPushes() {
    await this.http.post("/readPushes", {
      body: {
        pushIds: this.unreadPushIds
      }
    });
    forEach(this.list, l => {
      if (!l.read) {
        l.read = true;
      }
    });
  }
  addPush(data) {
    this.list.push(data);
  }
  setPushInfo(list) {
    this.list.replace(list);
  }
  clearList() {
    this.list.replace([]);
  }
}

export default PushInfo;
