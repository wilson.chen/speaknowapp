// @flow
import { observable } from "mobx";
import config from "../../Config";
import io from "socket.io-client";
import _ from "lodash";
import { showMessage, hideMessage } from "react-native-flash-message";

function disconnetLog() {
  if (global.loginTimestamp) {
    const mins = (+new Date() - window.loginTimestamp) / (1000 * 60);
    console.log(`Disconnect at ${new Date()}, duration ${mins}`);
  }
}

let reconnectCount = 0;

class Socket {
  @observable ins: Object;
  init() {
    return new Promise((resolve, reject) => {
      this.ins = io(config.ioEvn, {
        timeout: 20000,
        transports: ["websocket"]
      });
      this.ins.on("connect", () => {
        resolve();
      });
    });
  }
  emitAddUser({ auth, house }) {
    const ins = this.ins;
    ins.emit(
      "addUser",
      {
        ...auth.user,
        clientId: ins.id
      },
      unreadMessages => {
        if (!_.isEmpty(unreadMessages)) {
          _.forEach(unreadMessages, (list, k) => {
            const sender = _.find(house.friends, f => f._id === k);
            if (sender) {
              _.forEach(list, item => {
                const _mss = auth.makeMessage(
                  sender,
                  auth.user,
                  item.data,
                  "USER_MESSAGE",
                  "SUCCESS",
                  item.timeStamp
                );
                house.addMessage(_mss);
              });
            }
          });
        }
      }
    );
  }
  initSocketEvent({ auth, pushInfo, house, navigation }) {
    const ins = this.ins;
    ins.on("addFriend", msg => {
      pushInfo.addPush(msg);
    });
    ins.on("message", mss => {
      const _mss = auth.makeMessage(
        mss.user,
        auth.user,
        mss.data,
        "USER_MESSAGE",
        "SUCCESS",
        mss.timeStamp
      );
      house.addMessage(_mss);
    });
    ins.on("new_push", async type => {
      await auth.reloadUser();
      pushInfo.setPushInfo(auth.user.pushInfo);
      if (type === "ADD_FRIEND_PASS") {
        house.getUserFriends();
      }
    });

    ins.on("disconnect", async type => {
      disconnetLog();
      this.emitAddUser({ auth, house });
    });
    ins.on("reconnect", type => {
      // message.success("Reconnected!");
      console.log(`Reconnect...${reconnectCount++}`);
      ins.emit("addUser", {
        ...auth.user,
        clientId: ins.id
      });
    });
    ins.on("loginFromOtherPlace", async type => {
      console.log("disconnect: log from other place");
      showMessage({
        message: "Sorry, you have logined from other place!",
        type: "danger"
      });
      navigation.navigate("Login");
    });
  }
}

export default Socket;
