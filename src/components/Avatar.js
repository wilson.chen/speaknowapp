import React, { Component } from "react";
import { getAvatarUrl } from "../utils/media";

import { Layout, Avatar } from "@ui-kitten/components";

import {
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons
} from "@expo/vector-icons";

import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";

const UserAvatar = ({ avatarId, large }) => {
  if (avatarId) {
    return (
      <Layout>
        <Avatar source={{ uri: getAvatarUrl(avatarId) }} />
      </Layout>
    );
  }

  return (
    <Layout>
      <Ionicons name="md-person-add" />
    </Layout>
  );
};

export default UserAvatar;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    display: "flex",
    alignItems: "center"
  },
  avatar: {
    margin: "auto"
  },
  nameWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  textDisplay: {
    textAlign: "center"
  }
});
