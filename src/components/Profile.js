import React, { Component } from "react";

import { Layout, Text } from "@ui-kitten/components";
import {
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons
} from "@expo/vector-icons";
import Avatar from "./Avatar";

import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";

const Profile = ({ user }) => {
  const {
    nickName,
    accountName,
    email,
    avatarId,
    gender,
    address,
    signature
  } = user;
  const genderIcon =
    gender === "MALE" ? <AntDesign name="man" /> : <AntDesign name="woman" />;
  return (
    <Layout style={styles.container}>
      <Avatar style={styles.avatar} avatarId={avatarId} large />
      <Layout style={[styles.container, styles.nameWrapper]}>
        <Text>{`${accountName} (${nickName})`}</Text>
        <Layout>{genderIcon}</Layout>
      </Layout>
      <Layout style={styles.container}>
        <Text style={styles.textDisplay}>{email}</Text>
      </Layout>
      {address ? (
        <Layout style={styles.container}>
          <Text style={styles.textDisplay}>{address}</Text>
        </Layout>
      ) : null}
      {signature ? (
        <Layout style={styles.container}>
          <Text style={styles.textDisplay}>{signature}</Text>
        </Layout>
      ) : null}
    </Layout>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    display: "flex",
    alignItems: "center"
  },
  avatar: {
    margin: "auto"
  },
  nameWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  textDisplay: {
    textAlign: "center"
  }
});
