//@flow
import React, { Component } from "react";
import { Layout, Text } from "@ui-kitten/components";

import {
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons
} from "@expo/vector-icons";
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";

const Nothing = ({ name }) => {
  const content = {
    TALK: {
      icon: <AntDesign name="bulb1" size={30} color="#2980b9" />,
      title: "You have no Conversiton here"
    },
    PUSH: {
      icon: <AntDesign name="sound" size={30} color="#e67e22" />,
      title: "You have no notification here"
    },
    CONTACT: {
      icon: <AntDesign name="heart" size={30} color="#eb2f96" />,
      title: "Click '+' to add some friend"
    }
  };
  const res = content[name];
  if (!res) {
    return null;
  }

  return (
    <Layout style={styles.nothingWrapper}>
      {res.icon}
      <Text style={styles.title}>{res.title}</Text>
      <Text>{res.subTitle}</Text>
    </Layout>
  );
};

export default Nothing;

const styles = StyleSheet.create({
  nothingWrapper: {
    height: "100%",
    padding: 40,
    backgroundColor: "#fff",
    textAlign: "center",
    display: "flex",
    alignItems: "center"
  },
  title: {
    marginTop: 10
  }
});
