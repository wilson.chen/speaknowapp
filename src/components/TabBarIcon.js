import * as React from "react";

import Colors from "../constants/Colors";

export default function TabBarIcon({ component, name, focused }) {
  return React.createElement(component, {
    name,
    size: 25,
    style: { marginBottom: -3 },
    color: focused ? Colors.tabIconSelected : Colors.tabIconDefault
  });
}
