import Config from "../../Config";

export function getAvatarUrl(id) {
  return `${Config.staticFile}/avatars/${id}.jpeg`;
}
