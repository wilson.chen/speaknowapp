import config from "../../Config";
const postFetch = async (url, params) => {
  const location = `${config.evn}${url}`;
  const res = await fetch(location, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(params)
  });
  return res.json();
};

export default postFetch;
