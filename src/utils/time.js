import moment from "moment";
export function getMessageTime(m) {
  const ts = m.timeStamp;
  if (ts) {
    let date = moment(ts).format("MM-DD HH:mm");
    if (moment().isSame(moment(ts), "day")) {
      date = `Today ${moment(ts).format("HH:mm")}`;
    } else if (
      moment()
        .subtract(1, "day")
        .isSame(moment(ts), "day")
    ) {
      date = `Yesterday ${moment(ts).format("HH:mm")}`;
    }

    return date;
  }
  return null;
}
