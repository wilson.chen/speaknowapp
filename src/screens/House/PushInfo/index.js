// @flow
import React, { Component } from "react";
import { observable } from "mobx";
import moment from "moment";
import { observer, inject } from "mobx-react";
import { Button, Input, Text } from "@ui-kitten/components";
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";
import {
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons,
  Feather
} from "@expo/vector-icons";
import Nothing from "../../../components/Nothing";
import Profile from "../../../components/Profile";
import Avatar from "../../../components/Avatar";
import { map, isEmpty, reverse } from "lodash";

import { showMessage, hideMessage } from "react-native-flash-message";

class PushInfo extends Component {
  @observable resultUser = null;
  @observable searchValue = "";
  @observable currentPush = null;
  constructor(props) {
    super(props);
  }

  renderResult = res => {
    return null;
  };
  accept = p => async () => {
    const { http, auth, pushInfo, house } = this.props;
    const res = await this.props.http.post("/acceptAddUserRequest", {
      friendId: p.payload.friendId
    });
    if (res.result === "SUCCESS") {
      showMessage({
        message: "Add friend successfully!",
        type: "success"
      });
      await house.getUserFriends();
      await auth.reloadUser();

      pushInfo.setPushInfo(auth.user.pushInfo);
      this.currentPush = null;
    }
  };
  refuse = user => () => {};
  startTalk = p => () => {
    const { friend } = p.payload;
    this.props.house.directToRoom(friend._id);
  };
  openPush = p => () => {
    this.currentPush = p;
  };
  renderPushDetail = () => {
    const p = this.currentPush;
    const { friend } = p.payload;
    const context = this.getPushContext(p);

    return (
      <View style={styles.pushDetail}>
        <View style={styles.pushHeader}>
          <Feather
            name="arrow-left"
            size={25}
            onPress={() => {
              this.currentPush = null;
            }}
          />
        </View>
        <Profile style={styles.profile} user={friend} />
        <Text style={styles.contextText}>{context.text}</Text>
        <View style={styles.contextActionWrapper}>{context.action}</View>
      </View>
    );
  };
  getPushContext(p) {
    let res = {};

    switch (p.type) {
      case "ADD_FRIEND": {
        const { friend } = p.payload;
        res = {
          text: p.done
            ? `${friend.nickName} is your friend now!`
            : `${friend.nickName} want to add you as a friend`,
          action: p.done ? (
            <Button
              style={styles.contextAction}
              onPress={this.startTalk(p)}
              type="primary"
            >
              Talk
            </Button>
          ) : (
            <Button
              style={styles.contextAction}
              onPress={this.accept(p)}
              type="primary"
            >
              Accept
            </Button>
          )
        };
        break;
      }
      case "ADD_FRIEND_PASS": {
        const { friend } = p.payload;
        res = {
          text: `${friend.nickName} has passed your friend request!`,
          action: (
            <Button
              onPress={this.startTalk(p)}
              style={styles.contextAction}
              type="primary"
            >
              Talk
            </Button>
          )
        };
        break;
      }
      default:
        res = {};
    }
    return res;
  }
  renderFriendPush = p => {
    const { friend } = p.payload;
    const context = this.getPushContext(p);

    return (
      <TouchableOpacity
        style={styles.pushItem}
        onPress={this.openPush(p)}
        key={friend._id}
      >
        <Avatar avatarId={friend.avatarId} />

        <View style={styles.infoWrapper}>
          <View style={styles.info}>
            <Text numberOfLines={1}>{context.text}</Text>
          </View>

          <Text style={styles.pushInfoTime}>
            {p.timestamp ? moment(p.timestamp).fromNow() : null}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  render() {
    const list = map(reverse(this.props.pushInfo.list.slice()), p => {
      let res = null;
      switch (p.type) {
        case "ADD_FRIEND":
        case "ADD_FRIEND_PASS":
          res = this.renderFriendPush(p);
          break;
        default:
          res = null;
      }
      return res;
    });

    if (isEmpty(list)) {
      return <Nothing name="PUSH" />;
    }

    return (
      <View style={styles.pushWrapper}>
        {this.currentPush ? (
          this.renderPushDetail()
        ) : (
          <View style={styles.pushInfo}>{list}</View>
        )}
      </View>
    );
  }
}

export default inject("auth", "http", "pushInfo", "house")(observer(PushInfo));

const styles = StyleSheet.create({
  pushWrapper: {
    height: "100%",
    paddingVertical: 15,
    backgroundColor: "#fff"
  },
  pushInfo: {
    paddingVertical: 5
  },
  pushItem: {
    borderTopWidth: 0.5,
    borderTopColor: "#efedf4",
    padding: 8,
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },

  search: {
    display: "flex",
    flexDirection: "row"
  },
  searchInput: {
    flex: 8
  },
  pushDetail: {
    paddingTop: 0
  },
  pushHeader: {
    marginBottom: 100,
    paddingHorizontal: 10
  },
  profile: {
    marginTop: 100
  },
  contextText: {
    textAlign: "center"
  },
  contextActionWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  contextAction: {
    width: 150,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20
  },
  unReadMarker: {
    color: "#fff",
    backgroundColor: "red",
    borderRadius: 10,
    width: 20,
    height: 20,
    textAlign: "center"
  },
  infoWrapper: {
    fontSize: 12,

    fontWeight: "normal",
    overflow: "hidden",
    marginLeft: 10
  },
  info: {},
  pushInfoTime: {
    fontSize: 11,
    color: "#b5bcc2"
  }
});
