import React, { Component } from "react";
import { observable } from "mobx";

import { observer, inject } from "mobx-react";
import { isEmpty, trim, includes } from "lodash";

import { Layout, Avatar, Text, Input, Button } from "@ui-kitten/components";
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";

import { showMessage, hideMessage } from "react-native-flash-message";

import {
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons
} from "@expo/vector-icons";

import Profile from "../../../components/Profile";

class AddFriend extends Component {
  @observable resultUser = null;
  @observable searchValue = null;
  constructor(props) {
    super(props);
  }

  changeSearchValue = v => {
    this.searchValue = v;
  };
  search = async () => {
    if (isEmpty(trim(this.searchValue))) {
      this.resultUser = null;
      this.searchValue = "";
    } else {
      const res = await this.props.http.post("/searchUser", {
        accountName: this.searchValue
      });
      if (res.result === "SUCCESS") {
        if (res.user.accountName === this.props.auth.user.accountName) {
          this.resultUser = -2;
        } else if (includes(this.props.auth.user.friends, res.user._id)) {
          this.resultUser = -3;
        } else {
          this.resultUser = res.user;
        }
      } else if (res.result === "USER_NOT_EXIST") {
        this.resultUser = -1;
      }
    }
  };
  addUser = async () => {
    if (this.resultUser && this.resultUser !== -1) {
      const res = await this.props.http.post("/sendAddUserRequest", {
        friendId: this.resultUser._id,
        mss: "Please~"
      });
      if (res.result === "SUCCESS") {
        this.resultUser = null;
        this.searchValue = null;
        showMessage({
          message: "Request has been sent",
          type: "success"
        });
      }
    }
  };
  renderResult = res => {
    if (this.resultUser === -1) {
      return (
        <View style={styles.result}>
          <AntDesign size={60} name="frowno" color="#f57b51" />
          <Text style={styles.resultText}>Sorry, the user is not exit!</Text>
        </View>
      );
    } else if (this.resultUser === -2) {
      return (
        <View style={styles.result}>
          <AntDesign size={60} name="smile" color="#f57b51" />
          <Text style={styles.resultText}>Oops, you can not add yourself!</Text>
        </View>
      );
    } else if (this.resultUser === -3) {
      return (
        <View style={styles.result}>
          <AntDesign size={60} name="smileo" color="#f57b51" />
          <Text style={styles.resultText}>
            Oops, he(she) is already your friend!
          </Text>
        </View>
      );
    } else if (this.resultUser) {
      return (
        <View style={styles.resultUserWrapper} user={this.resultUser}>
          <Profile user={this.resultUser} />
          <Button style={styles.addBtn} onPress={this.addUser} name="primary">
            Add
          </Button>
        </View>
      );
    }

    return (
      <View style={styles.result}>
        <AntDesign size={60} name="smileo" color="#f57b51" />
        <Text style={styles.resultText}>Let's find some friends!</Text>
      </View>
    );
  };
  handleInputKey = e => {
    if (e.keyCode === 13) {
      this.search();
    }
  };
  render() {
    return (
      <View style={styles.addFriendsWrapper}>
        <View style={styles.search}>
          <Input
            style={styles.searchInput}
            onChangeText={this.changeSearchValue}
            value={this.searchValue}
            onPress={this.search}
            size="large"
          />
          <Button style={styles.searchBtn} onPress={this.search} name="primary">
            Search
          </Button>
        </View>
        {this.renderResult()}
      </View>
    );
  }
}

export default inject("auth", "http")(observer(AddFriend));

const styles = StyleSheet.create({
  addFriendsWrapper: {
    padding: 30,
    backgroundColor: "#fff",
    height: "100%"
  },
  resultText: {
    marginTop: 20
  },
  result: {
    padding: 20,
    marginTop: 100,
    textAlign: "center",
    display: "flex",
    alignItems: "center"
  },
  search: {
    display: "flex",
    flexDirection: "row"
  },
  searchInput: {
    flex: 8,
    marginRight: 10
  },
  searchBtn: {
    height: 48
  },
  resultUserWrapper: {
    paddingTop: 60,
    textAlign: "center",
    display: "flex",
    alignItems: "center"
  },
  addBtn: {
    width: 150
  }
});
