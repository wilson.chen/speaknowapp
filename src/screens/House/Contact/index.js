// @flow
import React, { Component } from "react";
import { isEmpty, map } from "lodash";
import { observable } from "mobx";

import { observer, inject } from "mobx-react";
import Avatar from "../../../components/Avatar";
import Profile from "../../../components/Profile";
import Nothing from "../../../components/Nothing";
import { Button, Input, Text, RadioGroup, Radio } from "@ui-kitten/components";

import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";

import {
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons,
  Feather
} from "@expo/vector-icons";

class Contact extends Component {
  @observable currentUser = null;
  @observable searchValue = "";
  constructor(props) {
    super(props);
  }
  sendMessage = () => {
    this.props.house.directToRoom(this.currentUser._id);
  };
  selectUser = user => () => {
    this.currentUser = user;
  };
  renderList() {
    const { friends } = this.props.house;
    if (isEmpty(friends)) {
      return <View>You have no friend here.</View>;
    }
    const list = map(friends, f => {
      return (
        <TouchableOpacity
          style={styles.friendcardWrapper}
          onPress={this.selectUser(f)}
          key={f._id}
        >
          <View style={styles.left}>
            <Avatar avatarId={f.avatarId} />
          </View>
          <View style={styles.right}>
            <Text style={styles.nickName}>{f.nickName}</Text>
            <Text style={styles.email}>{f.email}</Text>
          </View>
        </TouchableOpacity>
      );
    });

    return <View>{list}</View>;
  }
  renderProfile = () => {
    if (this.currentUser) {
      return (
        <View style={styles.pushDetail}>
          <View style={styles.pushHeader}>
            <Feather
              name="arrow-left"
              size={25}
              onPress={() => {
                this.currentUser = null;
              }}
            />
          </View>
          <Profile user={this.currentUser} />
          <Button
            style={styles.sendBtn}
            onPress={this.sendMessage}
            type="primary"
          >
            Send Message
          </Button>
        </View>
      );
    }

    return <View></View>;
  };

  render() {
    const { friends } = this.props.house;
    if (isEmpty(friends)) {
      return <Nothing name="CONTACT" />;
    }
    return (
      <View style={styles.contactWrapper}>
        <View>
          {this.currentUser ? this.renderProfile() : this.renderList()}
        </View>
      </View>
    );
  }
}

export default inject("auth", "http", "house")(observer(Contact));

const styles = StyleSheet.create({
  contactWrapper: {
    height: "100%",
    paddingVertical: 15,
    backgroundColor: "#fff"
  },
  pushInfo: {
    paddingVertical: 5
  },
  pushItem: {
    borderTopWidth: 0.5,
    borderTopColor: "#efedf4",
    padding: 8,
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  info: {
    marginLeft: 10
  },
  search: {
    display: "flex",
    flexDirection: "row"
  },
  searchInput: {
    flex: 8
  },
  pushDetail: {
    paddingTop: 0,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  pushHeader: {
    width: "100%",
    marginBottom: 100,
    paddingHorizontal: 10
  },
  profile: {
    marginTop: 100
  },
  contextText: {
    textAlign: "center"
  },
  contextActionWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  contextAction: {
    width: 150,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20
  },
  friendcardWrapper: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    height: 60,
    padding: 10,
    borderTopWidth: 0.5,
    borderTopColor: "#efedf4",
    fontWeight: "bold"
  },
  nickName: {
    fontWeight: "bold"
  },
  right: {
    flexGrow: 1,
    flexShrink: 1,
    marginLeft: 10,
    overflow: "hidden"
  },
  sendBtn: {
    width: 150
  },
  email: {
    fontSize: 11,
    color: "#b5bcc2"
  }
});
