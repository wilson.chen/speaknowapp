import React, { Component } from "react";
import { observable } from "mobx";

import { observer, inject } from "mobx-react";
import { isEmpty, map, reverse } from "lodash";
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";

import FriendCard from "./FriendCard";
import Chat from "./Chat";
import Nothing from "../../../components/Nothing";

class Talk extends Component {
  @observable currentUser = null;
  @observable searchValue = "";
  constructor(props) {
    super(props);
  }

  render() {
    const { house } = this.props;
    if (isEmpty(house.rooms)) {
      return <Nothing name="TALK" />;
    }
    const fiendsList = map(reverse(house.rooms.slice()), r => (
      <FriendCard room={r} key={r.id} />
    ));
    // console.log(house.rooms.slice());

    return (
      <View style={styles.talkWrapper}>
        <View>{fiendsList}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  talkWrapper: {
    height: "100%",
    backgroundColor: "#fff",
    paddingTop: 20
  },
  subText: {
    marginTop: 20
  },
  formWrapper: {
    paddingTop: 40
  },
  developmentModeText: {
    marginBottom: 20,
    color: "rgba(0,0,0,0.4)",
    fontSize: 14,
    lineHeight: 19,
    textAlign: "center"
  },
  contentContainer: {
    marginTop: 0,
    paddingVertical: 30
  },
  nameInput: {
    height: 50,
    backgroundColor: "rgba(0,0,0,0.05)"
  },
  passwordInput: {
    marginTop: 20
  },
  submitButton: {
    marginTop: 20
  }
});

export default inject("house")(observer(Talk));
