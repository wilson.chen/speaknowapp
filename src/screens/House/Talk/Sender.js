// @flow
import { isEmpty, trim } from "lodash";

import React, { Component } from "react";
import { observer, inject } from "mobx-react";

import { Layout, Avatar, Button, Input } from "@ui-kitten/components";

class Sender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };
  }
  handleInput = v => {
    this.setState({
      value: v
    });
  };
  submit = () => {
    if (isEmpty(trim(this.state.value))) {
      return;
    }

    const { house, socket, auth } = this.props;
    const target = house.currentRoom.target;
    const promise = new Promise((resolve, reject) => {
      socket.ins.emit(
        "message",
        {
          userId: target._id,
          msg: this.state.value
        },
        res => {
          resolve(res);
        }
      );
    });
    promise.then(v => {
      const _mss = auth.makeMessage(
        auth.user,
        target,
        this.state.value,
        "USER_MESSAGE",
        v.result,
        Date.now()
      );
      house.addMessage(_mss);
      this.setState({
        value: ""
      });
    });
  };

  render() {
    return (
      <Layout>
        <Input
          size="large"
          onChangeText={this.handleInput}
          value={this.state.value}
        />
        <Button
          type="primary"
          onPress={this.submit}
          disabled={isEmpty(trim(this.state.value))}
        >
          Send
        </Button>
      </Layout>
    );
  }
}
export default inject("auth", "http", "socket", "house")(observer(Sender));
