import { map } from "lodash";
import React, { Component } from "react";
import { reaction } from "mobx";
import { observer, inject } from "mobx-react";
import Avatar from "../../../components/Avatar";
import Sender from "./Sender";
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";

import { Layout, Text } from "@ui-kitten/components";
const Message = ({ message }) => {
  if (message.isSentByHost) {
    return (
      <Layout style={styles.messageWrapper}>
        <Text className="mss-word">{message.data}</Text>
        {message.status != "SUCCESS" ? (
          <Text className="failed-mark">!</Text>
        ) : null}
        <Avatar avatarId={message.sender.avatarId} />
      </Layout>
    );
  }
  return (
    <Layout>
      <Avatar avatarId={message.sender.avatarId} />
      <Text className="mss-word">{message.data}</Text>
    </Layout>
  );
};

class Chat extends Component {
  constructor(props) {
    super(props);
    // reaction(
    //   () => props.room.history.length,
    //   () => {
    //     if (this.refWindow) {
    //       setTimeout(() => {
    //         this.refWindow.scrollTop =
    //           this.refWindow.scrollHeight - this.refWindow.offsetHeight;
    //       }, 200);
    //     }
    //   }
    // );
  }
  componentDidUpdate() {
    // this.refWindow.scrollTop =
    //   this.refWindow.scrollHeight - this.refWindow.offsetHeight;
  }
  componentDidMount() {
    // this.refWindow.scrollTop =
    //   this.refWindow.scrollHeight - this.refWindow.offsetHeight;
  }
  componentWillUnmount() {
    this.props.house.currentRoomId = null;
  }

  renderChatWindow = () => {
    const room = this.props.house.currentRoom;

    const items = map(room.historyWithTimeStamp, (e, i) => {
      if (e.type === "TIMESTAMP") {
        return (
          <Layout className="item message-timestamp">
            <Text>{e.value}</Text>
          </Layout>
        );
      }
      return <Message message={e} key={i} />;
    });

    return (
      <Layout style={styles.chatWindow} ref={this.refChatWindow}>
        {items}
      </Layout>
    );
  };
  refChatWindow = ref => {
    this.refWindow = ref;
  };
  render() {
    const { room } = this.props;
    const { nickName, signature } = room.target;
    return (
      <Layout style={styles.chatWrapper}>
        <Layout className="chat-header">
          <Text>
            {nickName} {signature ? `(${signature})` : null}
          </Text>
        </Layout>
        {this.renderChatWindow()}
        <Sender />
      </Layout>
    );
  }
}
export default inject("auth", "http", "socket", "house")(observer(Chat));

const styles = StyleSheet.create({
  chatWrapper: {
    backgroundColor: "#fff",
    height: "100%"
  },
  chatWindow: {
    flexGrow: 1
  },
  messageWrapper: {
    display: "flex",
    flexDirection: "row",
    alignSelf: "flex-end"
  }
});
