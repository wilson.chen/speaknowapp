//@flow

import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import Unread from "../../../components/Unread";
import Avatar from "../../../components/Avatar";
import { getMessageTime } from "../../../utils/time";

import { Layout, Text } from "@ui-kitten/components";

import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";

class FriendCard extends Component {
  handleClick = () => {
    const { room, house } = this.props;
    house.currentRoomId = room.id;
    if (room.hasUnReadMessages) {
      room.readAll();
    }
  };
  renderUnReadNumber() {
    const { room, house } = this.props;
    if (room.hasUnReadMessages) {
      return (
        <View style={styles.unReadNumber}>
          <Text style={styles.unReadNumberText}>
            {room.unReadMessages.length}
          </Text>
        </View>
      );
    }

    return null;
  }
  render() {
    const { room, house } = this.props;
    const { currentRoomId } = house;
    const friend = room.target;
    const active = currentRoomId && currentRoomId === room.id;
    return (
      <TouchableOpacity
        style={styles.friendcardWrapper}
        onPress={this.handleClick}
      >
        <View style={styles.left}>
          <Avatar avatarId={friend.avatarId} />
        </View>

        <View style={styles.right}>
          <Text style={styles.nickName}>{friend.nickName}</Text>

          {room.lastMessage ? (
            <View style={styles.lastMessage}>
              <Text style={styles.lastMessageContent} numberOfLines={1}>
                {room.lastMessage.data}
              </Text>

              <Text style={styles.lastMessageTime}>
                {getMessageTime(room.lastMessage)}
              </Text>
            </View>
          ) : null}
        </View>
        {this.renderUnReadNumber()}
        {room.hasUnReadMessages ? <Unread /> : null}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  friendcardWrapper: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    height: 60,
    padding: 10,
    borderTopWidth: 0.5,
    borderTopColor: "#efedf4",
    fontWeight: "bold"
  },
  unReadNumber: {
    color: "#fff",
    backgroundColor: "red",
    borderRadius: 10,
    width: 20,
    height: 20,
    textAlign: "center"
  },
  unReadNumberText: {
    color: "#fff",
    fontSize: 12,
    textAlign: "center"
  },
  nickName: {
    fontWeight: "bold"
  },
  lastMessage: {
    display: "flex",
    flexDirection: "row",
    fontSize: 12,

    fontWeight: "normal",
    overflow: "hidden"
  },
  lastMessageContent: {
    overflow: "hidden",
    flexShrink: 1,
    color: "#b5bcc2"
  },
  lastMessageTime: {
    color: "#b5bcc2",
    textAlign: "center",
    fontSize: 12,
    marginLeft: 10,
    flexShrink: 0
  },
  right: {
    flexGrow: 1,
    flexShrink: 1,
    marginLeft: 10,
    overflow: "hidden"
  }
});

export default inject("house", "auth", "socket")(observer(FriendCard));
