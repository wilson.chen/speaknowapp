import React, { Component } from "react";
import { observable, toJS } from "mobx";

import { observer, inject } from "mobx-react";

import { View, StyleSheet } from "react-native";

import { map, toLower, find, forEach, capitalize, reverse } from "lodash";

import { Icon, Text } from "@ui-kitten/components";

import { getAvatarUrl } from "../utils/media";

import { GiftedChat } from "react-native-gifted-chat";

import { showMessage, hideMessage } from "react-native-flash-message";

import {
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons,
  Feather
} from "@expo/vector-icons";

function disconnetLog() {
  if (global.loginTimestamp) {
    const mins = (+new Date() - global.loginTimestamp) / (1000 * 60);
    console.log(`Disconnect at ${new Date()}, duration ${mins}`);
  }
}

class ChatRoom extends Component {
  onSend = messages => {
    const message = messages[0].text;
    const { house, socket, auth } = this.props;
    const target = house.currentRoom.target;
    const promise = new Promise((resolve, reject) => {
      socket.ins.emit(
        "message",
        {
          userId: target._id,
          msg: message
        },
        res => {
          resolve(res);
        }
      );
    });
    promise.then(v => {
      if (v.result === "USER_NOT_EXIST") {
        showMessage({
          message: `Sorry, user is not online now`,
          type: "danger"
        });
      } else if (v.result !== "SUCCESS") {
        showMessage({
          message: `Sorry, something is wrong!`,
          type: "danger"
        });
      }

      const _mss = auth.makeMessage(
        auth.user,
        target,
        message,
        "USER_MESSAGE",
        v.result,
        Date.now()
      );
      house.addMessage(_mss);
    });
  };

  render() {
    const { currentRoom } = this.props.house;
    if (!currentRoom) {
      return null;
    }
    const history = currentRoom.history;

    const data = map(history.slice(), (h, index) => {
      return {
        _id: index,
        text: h.data,
        createdAt: new Date(h.timeStamp),
        user: {
          ...h.sender,
          avatar: getAvatarUrl(h.sender.avatarId)
        }
      };
    });

    return (
      <View style={styles.charRoom}>
        <View style={styles.pushHeader}>
          <Feather
            name="arrow-left"
            size={25}
            onPress={() => {
              this.props.navigation.navigate("House");
              this.props.house.currentRoomId = null;
            }}
          />
          <Text style={styles.pushHeaderNickName}>
            {toJS(currentRoom.target).nickName}
            {` (${toJS(currentRoom.target).signature})`}
          </Text>
        </View>
        <GiftedChat
          messages={reverse(data)}
          onSend={this.onSend}
          user={toJS(this.props.auth.user)}
          showUserAvatar
        />
      </View>
    );
  }
}

export default inject(
  "auth",
  "http",
  "pushInfo",
  "house",
  "socket"
)(observer(ChatRoom));

const styles = StyleSheet.create({
  charRoom: {
    height: "100%",
    backgroundColor: "#fff"
  },
  pushHeader: {
    paddingLeft: 10,
    paddingTop: 45,
    paddingBottom: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#efedf4",
    backgroundColor: "#fff",
    display: "flex",
    flexDirection: "row"
  },
  pushHeaderNickName: {
    marginLeft: 20
  },
  chatRoom: {
    backgroundColor: "#fff"
  },
  mHeader: {
    display: "flex",
    flexDirection: "row"
  },
  container: {
    padding: 25,
    height: "100%"
  },
  signToggle: {
    textAlign: "center",
    fontSize: 17
  },
  title: {
    marginTop: 30
  }
});
