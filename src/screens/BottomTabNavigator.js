import React, { Component } from "react";
import { observable, toJS, autorun } from "mobx";

import { observer, inject } from "mobx-react";

import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";
import config from "../../Config";
import { map, toLower, find, forEach, capitalize, reverse } from "lodash";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import TabBarIcon from "../components/TabBarIcon";

import { Icon, Text, Button } from "@ui-kitten/components";

import AddFriend from "./House/AddFriend";
import Talk from "./House/Talk";
import Chat from "./House/Talk/Chat";
import PushInfo from "./House/PushInfo";
import Contact from "./House/Contact";
import { getAvatarUrl } from "../utils/media";
import ChatRoom from "./ChatRoom";
import Avatar from "../components/Avatar";
import Profile from "./Profile";

import {
  Ionicons,
  AntDesign,
  Feather,
  MaterialCommunityIcons,
  MaterialIcons
} from "@expo/vector-icons";

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = "Home";

function disconnetLog() {
  if (loginTimestamp.loginTimestamp) {
    const mins = (+new Date() - global.loginTimestamp) / (1000 * 60);
    console.log(`Disconnect at ${new Date()}, duration ${mins}`);
  }
}

class House extends Component {
  @observable friends = [];
  @observable currentRoom = null;
  @observable currentNav = "TALK";
  @observable openMyProfile = false;
  constructor(props) {
    super(props);
    if (!props.auth.user) {
      props.navigation.navigate("Login");
    } else {
      props.socket.init().then(async () => {
        await this.props.house.getUserFriends();
        this.props.house.initRooms();
        props.socket.emitAddUser(props);
        props.socket.initSocketEvent(props);
        this.initPushInfo();
      });
    }
  }
  componentDidMount() {
    autorun(() => {
      const { currentRoomId } = this.props.house;
      if (currentRoomId) {
        this.props.navigation.navigate("ChatRoom");
      }
    });
  }
  componentWillUnmount() {
    if (this.props.socket.ins) {
      this.props.socket.ins.disconnect();
      console.log("disconnect socket done");
    }
    this.props.house.resetHouse();
  }

  initPushInfo = () => {
    this.props.pushInfo.setPushInfo(this.props.auth.user.pushInfo || []);
  };

  renderChatRoom() {
    const { currentRoom } = this.props.house;
    if (currentRoom) {
      return <ChatRoom />;
    }

    return null;
  }
  renderMyProfile() {
    const { auth, navigation } = this.props;

    return (
      <Profile
        auth={auth}
        navigation={navigation}
        logout={() => {
          auth.logout();
          navigation.navigate("Login");
        }}
      />
    );
  }

  render() {
    if (!this.props.auth.user) {
      return null;
    }
    const navigatorPage = (
      <BottomTab.Navigator initialRouteName={"TALK"}>
        <BottomTab.Screen
          name="ADD"
          component={AddFriend}
          listeners={{
            tabPress: e => {
              this.props.house.currentNav = "ADD";
            }
          }}
          options={{
            title: "Add",
            tabBarIcon: ({ focused }) => (
              <TabBarIcon
                component={Ionicons}
                name="md-person-add"
                focused={focused}
              />
            )
          }}
        />
        <BottomTab.Screen
          name="TALK"
          component={Talk}
          listeners={{
            tabPress: e => {
              this.props.house.currentNav = "TALK";
            }
          }}
          options={{
            title: "Talk",
            tabBarIcon: ({ focused }) => (
              <TabBarIcon
                component={AntDesign}
                name="message1"
                focused={focused}
              />
            )
          }}
        />
        <BottomTab.Screen
          name="PushInfo"
          component={PushInfo}
          listeners={{
            tabPress: e => {
              this.props.house.currentNav = "Push Info";
            }
          }}
          options={{
            title: "Message",
            tabBarIcon: ({ focused }) => (
              <TabBarIcon
                component={MaterialCommunityIcons}
                name="email"
                focused={focused}
              />
            )
          }}
        />
        <BottomTab.Screen
          name="CONTACT"
          component={Contact}
          listeners={{
            tabPress: e => {
              this.props.house.currentNav = "Contact";
            }
          }}
          options={{
            title: "Contact",
            tabBarIcon: ({ focused }) => (
              <TabBarIcon
                component={MaterialIcons}
                name="group"
                focused={focused}
              />
            )
          }}
        />
      </BottomTab.Navigator>
    );

    if (this.openMyProfile) {
      return (
        <View style={styles.mHouse}>
          <View style={styles.pushHeader}>
            <Feather
              name="arrow-left"
              size={25}
              onPress={() => {
                this.openMyProfile = false;
              }}
            />
          </View>
          {this.renderMyProfile()}
        </View>
      );
    }

    return (
      <View style={styles.mHouse}>
        <View style={styles.mHeader}>
          <Text style={styles.subText} category="h6">
            {capitalize(this.props.house.currentNav)}
          </Text>
          <View style={styles.host}>
            <TouchableOpacity
              onPress={() => {
                this.openMyProfile = true;
              }}
            >
              <Avatar avatarId={this.props.auth.user.avatarId} />
            </TouchableOpacity>
          </View>
        </View>
        {navigatorPage}
      </View>
    );
  }
}

export default inject(
  "auth",
  "http",
  "pushInfo",
  "house",
  "socket"
)(observer(House));

const styles = StyleSheet.create({
  mHouse: {
    height: "100%"
  },
  pushHeader: {
    paddingLeft: 10,
    paddingTop: 45,
    paddingBottom: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#A9B0B7",
    backgroundColor: "#fff",
    display: "flex",
    flexDirection: "row"
  },
  mHeader: {
    width: "100%",
    height: 80,
    textAlign: "center",
    justifyContent: "center",
    position: "relative"
  },
  subText: {
    textAlign: "center",
    marginTop: 40
  },
  container: {
    padding: 25,
    height: "100%"
  },
  signToggle: {
    textAlign: "center",
    fontSize: 17
  },
  title: {
    marginTop: 30
  },
  host: {
    position: "absolute",
    right: 10,
    bottom: 10,
    width: 40,
    height: 40,
    borderRadius: 40,
    overflow: "hidden"
  }
});
