import React from "react";
import { Layout, Text, Avatar } from "@ui-kitten/components";
import { View, StyleSheet } from "react-native";

import Signin from "./Signin";
import Signup from "./Signup";
import { ScrollView } from "react-native-gesture-handler";
import logoImage from "../../../assets/images/logo.png";

export default function EntryLayout({ navigation }) {
  const [signinMode, setSigninMode] = React.useState(true);

  return (
    <ScrollView style={styles.container}>
      <View style={styles.logoWrapper}>
        <Avatar
          style={styles.logo}
          size="giant"
          shape="square"
          source={logoImage}
        />
      </View>

      {signinMode ? (
        <View>
          <Signin navigation={navigation} />
          <Text
            style={styles.signToggle}
            category="h2"
            onPress={() => {
              setSigninMode(false);
            }}
          >
            Sign Up
          </Text>
        </View>
      ) : (
        <View>
          <Signup
            navigation={navigation}
            callback={() => {
              setSigninMode(true);
            }}
          />
          <Text
            style={styles.signToggle}
            category="h2"
            onPress={() => {
              setSigninMode(true);
            }}
          >
            Sign In
          </Text>
        </View>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 25,
    height: "100%",
    backgroundColor: "#FAFAFA"
  },
  logoWrapper: {
    marginTop: 60,
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    width: 200,
    height: 200
  },
  signToggle: {
    textAlign: "center",
    fontSize: 17,
    color: "#5D6A76"
  },
  title: {
    marginTop: 30
  }
});
