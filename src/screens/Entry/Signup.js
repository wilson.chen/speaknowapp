import React, { useContext } from "react";
import _ from "lodash";
import { Button, Input, Text, RadioGroup, Radio } from "@ui-kitten/components";

import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import * as WebBrowser from "expo-web-browser";
import { showMessage, hideMessage } from "react-native-flash-message";

import { StoresContext } from "../../stores";

import { Formik } from "formik";

export default function Signup({ navigation, callback }) {
  const { http, auth } = useContext(StoresContext);

  const [pressCounter, setPressCounter] = React.useState(0);
  const onPress = async values => {
    const { accountName, nickName, email, password, gender } = {
      accountName: "wilson14",
      nickName: "wilson14",
      email: "wilson14@web.com",
      password: 111111,
      gender: "MALE"
    };
    const avatarId = `${gender === "MALE" ? "man" : "lady"}-${_.random(1, 8)}`;
    const res = await http.post("/register", {
      accountName,
      nickName,
      email,
      password,
      gender,
      avatarId
    });
    if (res.result === "SUCCESS") {
      showMessage({
        message: "Regist successfullty, please login!",
        type: "success"
      });
      callback();
    } else {
      showMessage({
        message: `Regist failed: ${res.result}, please try again!`,
        type: "danger"
      });
    }
  };

  const [userId, setUserId] = React.useState("");
  const [password, setPassword] = React.useState("");
  const initialValues = {
    email: "",
    accountName: "",
    nickName: "",
    password: "",
    passwordConfirm: "",
    gender: "MALE"
  };
  const GENDERS = ["MALE", "FEMALE"];
  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <Text style={styles.subText} category="h6">
          Please Sign up
        </Text>

        <Formik initialValues={initialValues} onSubmit={onPress}>
          {({ handleChange, handleBlur, handleSubmit, values }) => (
            <View style={styles.formWrapper}>
              <RadioGroup
                style={[styles.gender, styles.field]}
                selectedIndex={_.findIndex(GENDERS, values.gender)}
                onChange={index => {
                  handleChange("gender")(GENDERS[index]);
                }}
              >
                <Radio style={styles.radio} text="Male" />
                <Radio style={styles.radio} text="Female" />
              </RadioGroup>
              <Input
                style={styles.field}
                placeholder="Account Name"
                onChangeText={handleChange("accountName")}
                onBlur={handleBlur("accountName")}
                value={values.accountName}
              />
              <Input
                style={styles.field}
                placeholder="Nick Name"
                onChangeText={handleChange("nickName")}
                onBlur={handleBlur("nickName")}
                value={values.nickName}
              />
              <Input
                style={styles.field}
                placeholder="Email"
                onChangeText={handleChange("email")}
                onBlur={handleBlur("email")}
                value={values.email}
              />
              <Input
                style={[styles.field, styles.password]}
                placeholder="Password"
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                value={values.password}
              />
              <Input
                style={styles.field}
                placeholder="Confirm Password"
                onChangeText={handleChange("passwordConfirm")}
                onBlur={handleBlur("passwordConfirm")}
                value={values.passwordConfirm}
              />
              <Button
                style={styles.submitButton}
                onPress={handleSubmit}
                title="Submit"
              >
                SIGN UP
              </Button>
            </View>
          )}
        </Formik>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    color: "#fff",
    paddingTop: 15
  },
  subText: {
    marginTop: 20
  },
  formWrapper: {
    paddingTop: 15
  },
  field: {
    marginTop: 15
  },
  gender: {
    display: "flex",
    flexDirection: "row"
  },
  password: {
    marginTop: 30
  },
  developmentModeText: {
    marginBottom: 20,
    color: "rgba(0,0,0,0.4)",
    fontSize: 14,
    lineHeight: 19,
    textAlign: "center"
  },
  contentContainer: {
    marginTop: 0,
    paddingVertical: 30
  },
  nameInput: {
    height: 50,
    backgroundColor: "rgba(0,0,0,0.05)"
  },
  passwordInput: {
    marginTop: 20
  },
  submitButton: {
    marginTop: 20
  }
});
