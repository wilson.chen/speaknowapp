import React, { useContext } from "react";
import { Button, Input, Text } from "@ui-kitten/components";
import { isEmpty } from "lodash";
import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import * as WebBrowser from "expo-web-browser";

import { StoresContext } from "../../stores";

import { showMessage, hideMessage } from "react-native-flash-message";

import { Layout, Avatar } from "@ui-kitten/components";
export default function Signin({ navigation }) {
  const { http, auth, house } = useContext(StoresContext);
  const [userId, setUserId] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [test, setTest] = React.useState(false);

  const validate = () => {
    let res;
    if (isEmpty(userId)) {
      res = "account should not be empty";
    } else if (isEmpty(password)) {
      res = "password should not be empty";
    }
    return res;
  };

  const onPress = async () => {
    const check = validate();
    if (check) {
      showMessage({
        message: check,
        type: "danger"
      });
    } else {
      const res = await http.post("/login", {
        accountName: userId,
        password: password
      });
      if (res.result === "SUCCESS") {
        auth.setUser(res.user);
        auth.setToken(res.token);
        house.setHost(res.user);
        navigation.navigate("House");
      } else {
        showMessage({
          message: "Invalid account or password",
          type: "danger"
        });
      }
    }
  };
  const onPressWilson = async () => {
    const res = await http.post("/login", {
      accountName: "wilson",
      password: 111111
    });
    auth.setUser(res.user);
    auth.setToken(res.token);
    house.setHost(res.user);
    navigation.navigate("House");
    global.loginTimestamp = +new Date();
  };
  const onPressChen = async () => {
    const res = await http.post("/login", {
      accountName: "chen1229",
      password: 111111
    });
    auth.setUser(res.user);
    auth.setToken(res.token);
    house.setHost(res.user);
    navigation.navigate("House");
    global.loginTimestamp = +new Date();
  };
  const onPress1989 = async () => {
    const res = await http.post("/login", {
      accountName: "chenmengkai1989",
      password: 111111
    });
    auth.setUser(res.user);
    auth.setToken(res.token);
    house.setHost(res.user);
    navigation.navigate("House");
    global.loginTimestamp = +new Date();
  };

  return (
    <View style={styles.container}>
      <View
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <Text style={styles.subText} category="h6">
          Please login
        </Text>

        <View style={styles.formWrapper}>
          <Input
            placeholder="Name"
            value={userId}
            onChangeText={setUserId}
            autoCapitalize="none"
          />
          <Input
            placeholder="Password"
            value={password}
            style={styles.passwordInput}
            onChangeText={setPassword}
            autoCapitalize="none"
          />
          {test ? (
            <View>
              <Button
                style={styles.submitButton}
                onPress={onPressWilson}
                status="info"
              >
                Sign in wilson
              </Button>
              <Button
                style={styles.submitButton}
                onPress={onPressChen}
                status="info"
              >
                Sign in chen
              </Button>
              <Button
                style={styles.submitButton}
                onPress={onPress1989}
                status="info"
              >
                Sign in 1989
              </Button>
            </View>
          ) : null}
          <Button style={styles.submitButton} onPress={onPress} status="info">
            SIGN IN
          </Button>
        </View>

        <Text
          onPress={() => {
            setTest(!test);
          }}
          style={{ marginTop: 50 }}
        >
          Toggle test
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 15
  },
  subText: {
    marginTop: 20
  },
  formWrapper: {
    paddingTop: 40
  },
  contentContainer: {
    marginTop: 0,
    paddingVertical: 30
  },
  passwordInput: {
    marginTop: 20
  },
  submitButton: {
    marginTop: 20
  }
});
