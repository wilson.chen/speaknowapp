import React, { Component, useState } from "react";
import { trim } from "lodash";
import { Layout, Text, Button, Input } from "@ui-kitten/components";
import {
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  MaterialIcons
} from "@expo/vector-icons";
import Avatar from "../components/Avatar";

import {
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput
} from "react-native";

import { showMessage, hideMessage } from "react-native-flash-message";

const Profile = ({ auth, navigation, logout }) => {
  const {
    nickName,
    accountName,
    email,
    avatarId,
    gender,
    address,
    signature
  } = auth.user;
  const genderIcon =
    gender === "MALE" ? <AntDesign name="man" /> : <AntDesign name="woman" />;
  const [signatureEditMode, setSignatureEditMode] = useState(false);
  const [signatureText, setSignatureText] = useState(signature);
  const handleCancelSig = () => {
    setSignatureEditMode(false);
    setSignatureText(signature);
  };
  const handleSaveSig = async () => {
    if (trim(signatureText) && trim(signatureText) !== signature) {
      console.log(123, trim(signatureText));
      const res = await auth.updateProfile(signatureText);
      if (res.result === "SUCCESS") {
        setSignatureEditMode(false);
        showMessage({
          message: "Your signature changed successfully",
          type: "success"
        });
      }
    }
  };
  return (
    <Layout style={styles.mainProfileWrapper}>
      <Avatar style={styles.avatar} avatarId={avatarId} large />
      <Layout style={[styles.container, styles.nameWrapper]}>
        <Text>{`${accountName} (${nickName})`}</Text>
        <Layout>{genderIcon}</Layout>
      </Layout>
      <Layout style={styles.container}>
        <Text style={styles.textDisplay}>{email}</Text>
      </Layout>
      {address ? (
        <Layout style={styles.container}>
          <Text style={styles.textDisplay}>{address}</Text>
        </Layout>
      ) : null}
      <Layout>
        {signatureEditMode ? (
          <Layout>
            <Input
              onChangeText={v => {
                setSignatureText(v);
              }}
              maxlength="30"
              value={signatureText}
            />
            <Layout style={styles.actions}>
              <Button style={styles.saveSigBtn} onPress={handleSaveSig}>
                Save
              </Button>
              <Button style={styles.cancelSigBtn} onPress={handleCancelSig}>
                Cancel
              </Button>
            </Layout>
          </Layout>
        ) : (
          <Layout>
            <Text>{signatureText}</Text>

            <Button
              style={styles.editSignatureBtn}
              onPress={() => {
                setSignatureEditMode(true);
              }}
            >
              Edit Signature
            </Button>
          </Layout>
        )}
        <Button style={styles.logoutBtn} onPress={logout}>
          Log out
        </Button>
      </Layout>
    </Layout>
  );
};

export default Profile;

const styles = StyleSheet.create({
  mainProfileWrapper: {
    height: "100%",
    display: "flex",
    alignItems: "center",
    padding: 20
  },
  actions: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    marginTop: 10
  },
  cancelSigBtn: {
    marginLeft: 20
  },
  container: {
    padding: 20,
    display: "flex",
    alignItems: "center"
  },
  avatar: {
    margin: "auto"
  },
  nameWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  textDisplay: {
    textAlign: "center"
  },
  editSignatureBtn: {
    marginTop: 20
  },
  logoutBtn: {
    marginTop: 30
  }
});
