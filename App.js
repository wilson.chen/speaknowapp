import * as React from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";
import { SplashScreen } from "expo";
import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import useLinking from "./src/useLinking";

import { ApplicationProvider, Layout, Text } from "@ui-kitten/components";
import { mapping, light as lightTheme } from "@eva-design/eva";

import EntryLayout from "./src/screens/Entry/EntryLayout";
import BottomTabNavigator from "./src/screens/BottomTabNavigator";
import ChatRoom from "./src/screens/ChatRoom";

import { configureStore, StoresContext } from "./src/stores";

import { Provider, observer } from "mobx-react";
import FlashMessage from "react-native-flash-message";

const Stack = createStackNavigator();
const store = configureStore();

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const containerRef = React.useRef();
  const { getInitialState } = useLinking(containerRef);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();

        // Load our initial navigation state
        setInitialNavigationState(await getInitialState());

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          "space-mono": require("./assets/fonts/SpaceMono-Regular.ttf")
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hide();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return null;
  } else {
    return (
      <ApplicationProvider mapping={mapping} theme={lightTheme}>
        <StoresContext.Provider value={store}>
          <Provider {...store}>
            <NavigationContainer>
              <Stack.Navigator headerMode="none">
                <Stack.Screen name="Login" component={EntryLayout} />
                <Stack.Screen name="House" component={BottomTabNavigator} />
                <Stack.Screen name="ChatRoom" component={ChatRoom} />
              </Stack.Navigator>
            </NavigationContainer>
            <FlashMessage position="top" />
          </Provider>
        </StoresContext.Provider>
      </ApplicationProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
