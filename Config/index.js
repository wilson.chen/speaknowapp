const evn = {
  home: "http://192.168.0.101",
  work: "http://192.168.2.249",
  pro: "http://140.143.224.99",
  localhost: "http://localhost"
};

const config = {
  evn: `${evn.pro}:3004`,
  ioEvn: `${evn.pro}:3002`,
  staticFile: "https://speaknow-1256217615.cos.ap-shanghai.myqcloud.com",
  appConfig: {
    MESSAGE_STORE_NUMBER: 50,
    MESSAGE_SHOW_TIMESTAMO_INTERVAL: 2 * 60 * 1000,
    LOCALSTORAGE_ROOM_NUMBER: 10
  }
};

export default config;
